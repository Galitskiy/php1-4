<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
?>

<?php Pjax::begin([
    'id' => 'world-form-container'
]); ?>
<?php $form = ActiveForm::begin([
    'options' => ['data' => ['pjax' => true],],
    'id' => 'world-form'
]); ?>


<?= $form->field($model, 'continent_id')->dropDownList(ArrayHelper::map($continents, 'continent_id', 'name'), [
    'id' => 'field-continent-id',
    'onchange' => '
        $("#field-region-id").val("");
        $("#field-city-id").val("");
        $("#world-form").submit()'
]) 
?>



<?php if($continent > 0): ?>
    <h2><?= $continent['name'] ?></h2>
    <p><?= $continent['description'] ?></p>
<?php endif; ?>



<?php if($continent > 0){
    echo $form->field($model, 'country_id')->dropDownList(ArrayHelper::map($countries, 'country_id', 'name'), [
        'id' => 'field-country-id',
        'onchange' => '
            $("#field-region-id").val("");
            $("#field-city-id").val("");
            $("#world-form").submit()']);
    }
?>



<?php if($model->country_id == '227'){
    echo $form->field($model, 'region_id')->dropDownList(ArrayHelper::map($regions, 'region_id', 'name_language'), [
        'id' => 'field-region-id',
        'onchange' => '$("#world-form").submit()']);
    }
?>



<?php 
    //var_dump($model);
    if($model->region_id > 1000){
    echo $form->field($model, 'city_id')->dropDownList(ArrayHelper::map($cities, 'city_id', 'name_language'), [
        'id' => 'field-city-id',
        'onchange' => ' $("#world-form").submit()']);
    }
?>



<?php ActiveForm::end(); ?>
<?php Pjax::end(); ?>
