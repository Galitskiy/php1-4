<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CountrySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="country-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'country_id') ?>

    <?= $form->field($model, 'code') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'official_name') ?>

    <?= $form->field($model, 'iso3') ?>

    <?php // echo $form->field($model, 'number') ?>

    <?php // echo $form->field($model, 'currency') ?>

    <?php // echo $form->field($model, 'capital') ?>

    <?php // echo $form->field($model, 'area') ?>

    <?php // echo $form->field($model, 'continent_id') ?>

    <?php // echo $form->field($model, 'coords') ?>

    <?php // echo $form->field($model, 'display_order') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
